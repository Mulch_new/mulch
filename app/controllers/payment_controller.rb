class PaymentController < ApplicationController
  def checkout
    if params[:payment_type] == "paypal"
      contributions = params[:total_contributions].to_f * get_freq_count(params[:freq])
      total_contributions = sprintf( "%0.02f", contributions + (contributions * 0.029) + 0.49).to_f
      
      PayPal::SDK.configure(
        :mode      => "sandbox",  # Set "live" for production
        :app_id    => 'APP-80W284485P519543T',
        :username  => Figaro.env.mulch_paypal_username,
        :password  => Figaro.env.mulch_paypal_password,
        :signature => Figaro.env.mulch_paypal_signature)
      @api = PayPal::SDK::AdaptivePayments.new
      
      session_key = "#{current_user.id}M#{Time.now.to_i}"
      session[session_key] = {
        freq: params[:freq],
        child: params[:child],
        coffee_cups: params[:coffee_cups].join(','),
        how_many: params[:howmanyCups].reject! { |c| c.empty? }.join(','),
        contribution: contributions,
        service_fee: (contributions * 0.029),
        transaction_fee: 0.49,
        total_contributions: total_contributions
      }

      # Build request object
      @pay = @api.build_pay({
        :actionType => "PAY",
        :cancelUrl => "http://mulch1948.herokuapp.com/payment/#{session_key}/cancel",
        :currencyCode => "USD",
        :feesPayer => "SENDER",
        :ipnNotificationUrl => "http://mulch1948.herokuapp.com/payment/#{session_key}/ipn_notify",
        :receiverList => {
        :receiver => [{
        :amount => total_contributions,
        :start_date => Date.today,
        :frequency => 6,
        :email => "platfo_1255612361_per@gmail.com" }] },
        :returnUrl => "http://mulch1948.herokuapp.com/payment/#{session_key}/success" })
        
        # Make API call & get response
        @response = @api.pay(@pay)
      
      # Access response
      if @response.success?
        @response.payKey
        redirect_to @api.payment_url(@response)  # Url to complete payment
      else
        @response.error[0].message
        redirect_to payment_cancle_path  # Url to complete payment
      end
    else
      @child = User.find(params["child"])
      @payer = current_user
      @contribution = {
        freq: params[:freq],
        child: params[:child],
        coffee_cups: params[:coffee_cups].join(','),
        how_many: params[:howmanyCups].reject! { |c| c.empty? }.join(','),
        total_contributions: params[:total_contributions]
      }
      @flash = { status: :success, message: "Your detail has been received! We will contact you soon!!" }
      UsersMailer.payment_success(response, current_user).deliver
      UsersMailer.pay_in_person(nil, current_user).deliver
    end
  end

  # paypal success url
  def success
    if is_valid_callback? params[:id], current_user
      @child = User.find(session[params[:id]]["child"])
      @payer = User.find(params[:id].split('M')[0])
      @flash = { status: :success, message: "Contribution payment successfully done!" }
      @contribution = session[params[:id]]
      session[params[:id]]["success"] = true
      destroy_payment_session params[:id], "success"
    else
      @flash = { status: :error, message: "Session is not longer available!" }
    end
  end

  # paypal cancele url
  def cancle
    if is_valid_callback? params[:id], current_user
      @flash = { status: :success, message: "Contribution payment successfully done!" }
      session[params[:id]]["cancle"] = true
      destroy_payment_session params[:id], "cancle"
    else
      @flash = { status: :error, message: "Session is not longer available!" }
    end
  end

  # paypal instant payment notification url
  def ipn_notify
    if is_valid_callback? params[:id], current_user
      PaymentMailer.ipn_notify(params, current_user, session[params[:id]]).deliver
      session[params[:id]]["ipn_notify"] = true
      destroy_payment_session params[:id], "ipn_notify"
    end
  end

  private 
    # check is valid callback from paypal
    def is_valid_callback? callback_id, user
      user_id = callback_id.split('M')[0]
      return true if user_id.to_i == user.id && !session[callback_id].nil?
      return false
    end

    # destroy payment session
    def destroy_payment_session session_key, status
      session[session_key] = nil if (session[session_key]["success"] && session[session_key]["ipn_notify"]) || session[session_key][status]
    end

    def get_freq_count freq
      if freq == 'one_time'
        return 1;
      elsif freq == 'monthly'
        return 1;
      elsif freq == 'quarterly'
        return 3;
      elsif freq == 'bi_annual'
        return 6;
      elsif freq == 'annually'
        return 12;
      end
    end
end
