class RegistrationsController < Devise::RegistrationsController

	def new
		@ref = User.find_by_ref_id(params[:ref_id]) rescue nil
		super
	end

	def create
		params[:user][:referral_id] = User.find_by_ref_id(params[:user][:ref_id]).try(:id) rescue nil
		params[:user][:ref_id] = nil
		super
	end
  	
  	protected

  		def after_sign_up_path_for(resource)
			UsersMailer.welcome_message(resource).deliver
			flash.delete(:notice)
			if session[:ref_id].nil?
				update_profile_path
			else
				"/pledge-now?ref_id=#{session[:ref_id]}"
			end
		end
end