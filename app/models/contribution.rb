# == Schema Information
#
# Table name: contributions
#
#  id                 :integer          not null, primary key
#  child_id           :integer
#  user_id            :integer
#  frequency_id       :integer
#  total_contribution :integer
#  payment_id         :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class Contribution < ActiveRecord::Base
end
