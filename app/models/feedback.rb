# == Schema Information
#
# Table name: feedbacks
#
#  id         :integer          not null, primary key
#  email      :string(255)
#  message    :text
#  phone      :string(255)
#  subject    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Feedback < ActiveRecord::Base
end
