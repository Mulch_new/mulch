class AddColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :mobile, :string
    add_column :users, :secondary_email, :string
    add_column :users, :age_role, :string
  end
end
